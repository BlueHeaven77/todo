module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  configureWebpack: {
    resolve: {
      extensions: ['*', '.js', '.vue', '.json']
    }
  }
}