import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import TaskListIndex from '@/components/TaskList/index.vue';

describe('component: TaskListIndex.vue', () => {
  let localVue;
  let vuetify;
  let wrapper;
  const div = document.createElement('div');
  const data = {
    search: '',
  };

  beforeAll(() => {
    localVue = createLocalVue(); // because of vuetify, we should use a localVue instance
    vuetify = new Vuetify();
    document.body.appendChild(div);

    wrapper = mount(TaskListIndex, {
      propsData: data,
      localVue,
      vuetify,
      attachTo: div,
    });
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  })
});
