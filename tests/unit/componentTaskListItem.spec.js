import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import TaskListItem from '@/components/TaskList/TaskListItem.vue';

describe('component: TaskListItem.vue', () => {
  let localVue;
  let vuetify;
  let wrapper;
  const div = document.createElement('div');
  const data = {
    task: {
      dueDate: 'dueDate',
      isComplete: true,
      note: 'note',
      priority: 'priority',
      title: 'title',
    }
  };

  beforeEach(() => {
    localVue = createLocalVue(); // because of vuetify, we should use a localVue instance
    vuetify = new Vuetify();
    document.body.appendChild(div);

    wrapper = mount(TaskListItem, {
      propsData: data,
      localVue,
      vuetify,
      attachTo: div,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  })

  describe('method: deleteTask', () => {
    it('should emit delete-task event with selected event as payload when confirming deletion', () => {
      wrapper.vm.deleteTask(data.task);
      expect(wrapper.emitted('delete-task')).toBeTruthy();
      expect(wrapper.emitted('delete-task')[0][0]).toEqual(data.task);
    });
  
    it('should close delete dialog when confirming deletion', () => {
      wrapper.vm.deleteTask(data.task);
      expect(wrapper.vm.isdeleteTaskDialogOpen).toBe(false);
    });
  });

  describe('method: formatDate', () => {
    it('should return formatted date as DD.MM.YYYY if input date is non-empty (expected ISO format)', () => {
      const formattedDate = wrapper.vm.formatDate('1977-03-25');
      expect(formattedDate).toEqual('25.03.1977');
    });

    it('should return an empty string if input date is empty or not a string', () => {
      const formattedDateEmptyString = wrapper.vm.formatDate('');
      const formattedDateNonString = wrapper.vm.formatDate({});
      expect(formattedDateEmptyString).toEqual('');
      expect(formattedDateNonString).toEqual('');
    });
  });
});
