import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import axios from 'axios';
import flushPromises from 'flush-promises';
import Tasks from '@/views/Tasks.vue';

jest.mock('axios');

describe('view: Tasks.vue', () => {
  let localVue;
  let vuetify;
  let wrapper;
  let axiosInstance;
  const div = document.createElement('div');
  const tasks = { 
    data: [
      {
        dueDate: '2020-08-30',
        id: 1,
        isComplete: true,
        note: 'note 1',
        priority: 1,
        title: 'title 1',
        userId: 1,
      }, {
        dueDate: '2020-08-30',
        id: 2,
        isComplete: true,
        note: 'note 2',
        priority: 2,
        title: 'title 2',
        userId: 1,
      }, {
        dueDate: '2020-09-29',
        id: 3,
        isComplete: false,
        note: 'note 3',
        priority: 3,
        title: 'title 3',
        userId: 1,
      }, {
        dueDate: '2020-09-11',
        id: 4,
        isComplete: false,
        note: 'note 4',
        priority: 4,
        title: 'title 4',
        userId: 1,
      }
    ],
    lastID: 3,
    modified: 1599764512807
  };

  beforeEach(() => {
    localVue = createLocalVue(); // because of vuetify, we should use a localVue instance
    axiosInstance = localVue.prototype.$http;
    localVue.prototype.$http = {
      get: jest.fn(() => {
        return Promise.resolve({ data: tasks.data });
      }),
      put: jest.fn(() => {
        return Promise.resolve();
      }),
      delete: jest.fn(() => {
        return Promise.resolve();
      }),
    };
    vuetify = new Vuetify();
    document.body.appendChild(div);

    wrapper = mount(Tasks, {
      propsData: { tasks: [] },
      localVue,
      vuetify,
      attachTo: div,
      stubs: {
        TaskEditor: true
      }
    });
  });

  afterEach(() => {
    localVue.prototype.$http = axiosInstance;
    jest.clearAllMocks();
    wrapper.destroy();
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  });

  describe('computed property: filteredTasks', () => {
    it('should filter the task list on title or note according to the searchTerm when the searchTerm data input is provided', () => {
      wrapper.vm.searchTerm = 'title 3';
      const expectedFilteredTaskList = [
        {
          dueDate: '2020-09-29',
          id: 3,
          isComplete: false,
          note: 'note 3',
          priority: 3,
          title: 'title 3',
          userId: 1,
        }
      ];

      expect(wrapper.vm.filteredTasks).toStrictEqual(expectedFilteredTaskList);
    });
  });

  describe('method: onTaskAdd', () => {
    it('should assign to taskToEdit an empty task data set (with the only exception of priority-related data) and open the taskToEdit modal', () => {
      const expectedTaskToEdit = { 
        priority: wrapper.vm.tasks.length + 1,
        priorityRange: wrapper.vm.tasks.length + 1,
      };
      wrapper.vm.onTaskAdd(true);

      expect(wrapper.vm.taskToEdit).toStrictEqual(expectedTaskToEdit);
      expect(wrapper.vm.isModalOpen).toBe(true);
    });
  });

  describe('method: onTaskDelete', () => {
    it('should call the deletion API to remove the input task from the data set', () => {
      const task = wrapper.vm.tasks.find((task) => task.id === 1);
      wrapper.vm.updateTask = jest.fn();
      const spy = jest.spyOn(wrapper.vm.$http, 'delete');
      wrapper.vm.onTaskDelete(task);

      expect(spy).toHaveBeenCalledWith(`/api/tasks/${task.id}`);
    });

    it('should call updateTask on toggle API call success', async () => {
      const task = wrapper.vm.tasks.find((task) => task.id === 1);
      wrapper.vm.updateTask = jest.fn();
      const spy = jest.spyOn(wrapper.vm, 'updateTask');
      wrapper.vm.onTaskDelete(task);
      await flushPromises();

      expect(spy).toHaveBeenCalledWith(task.id, null);
    });
  });

  describe('method: onTaskStatusToggle', () => {
    it('should call the update API to toggle the status of the input task from complete to incomplete or vice versa', () => {
      const task = wrapper.vm.tasks.find((task) => task.id === 1);
      const expectedPayload = { ...task, isComplete: !task.isComplete };
      const spy = jest.spyOn(wrapper.vm.$http, 'put');
      wrapper.vm.onTaskStatusToggle(task);

      expect(spy).toHaveBeenCalledWith('/api/tasks', expectedPayload);
    });

    it('should call updateTask on toggle API call success', async () => {
      const task = wrapper.vm.tasks.find((task) => task.id === 1);
      const expectedPayload = { ...task, isComplete: !task.isComplete };
      const spy = jest.spyOn(wrapper.vm, 'updateTask');
      wrapper.vm.onTaskStatusToggle(task);
      await flushPromises();

      expect(spy).toHaveBeenCalledWith(task.id, expectedPayload);
    });
  });

  describe('method: onTaskEdit', () => {
    it('should assign task data to taskToEdit, calculate and assign property prorityRange and open the TaskEditor modal', () => {
      const task = wrapper.vm.tasks.find((task) => task.id === 1);
      const expectedTaskToEdit = { ...task, priorityRange: wrapper.vm.tasks.length };
      wrapper.vm.onTaskEdit(task);

      expect(wrapper.vm.taskToEdit).toStrictEqual(expectedTaskToEdit);
      expect(wrapper.vm.isModalOpen).toBe(true);
    });
  });

  describe('method: populateTask', () => {
    it('should get task list from the API and return it to data tasks', async () => {
      wrapper.vm.populateTasks();
      await flushPromises();

      expect(wrapper.vm.tasks).toStrictEqual(tasks.data);
    });

    it('should set isLoading to true before the API call and to false when the API has returned', async () => {
      wrapper.vm.populateTasks();
      
      expect(wrapper.vm.isLoading).toBe(true);
      await flushPromises();
      expect(wrapper.vm.isLoading).toBe(false);
    });
  });

  describe('method: updateTask', () => {
    it('should remove task based on the input id when input data is not provided', () => {
      wrapper.vm.updateTask(1);

      expect(wrapper.vm.tasks).toStrictEqual(tasks.data);
    });

    it('should update a task based on the input id when input data is provided', () => {
      const newData = {
        dueDate: '2020-08-30',
        id: 2,
        isComplete: true,
        note: 'note 2',
        priority: 2,
        title: 'updated title 2',
        userId: 1,
      };
      wrapper.vm.updateTask(2, newData);
      const task = wrapper.vm.tasks.find((task) => task.id === 2);

      expect(task.title).toStrictEqual(newData.title);
    });
  });
});
