import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import axios from 'axios';
import flushPromises from 'flush-promises';
import TaskEditor from '@/components/TaskEditor.vue';

jest.mock('axios');

describe('component: TaskEditor.vue', () => {
  let localVue;
  let vuetify;
  let wrapper;
  let axiosInstance;
  const div = document.createElement('div');
  const data = {
    dueDate: 'dueDate',
    isComplete: true,
    note: 'note',
    priority: 'priority',
    priorityRange: 5,
    title: 'title',
  };

  beforeEach(() => {
    localVue = createLocalVue(); // because of vuetify, we should use a localVue instance
    axiosInstance = localVue.prototype.$http;
    vuetify = new Vuetify();
    document.body.appendChild(div);

    /*
    to render a vuetify dialog, vuetify requires it to be placed inside the v-app component,
    so we pack our component into a 'bridge' component
    */
    const App = localVue.component('App', {
      components: { TaskEditor },
      data() {
        return { data: data };
      },
      template: `
        <v-app>
          <TaskEditor
            :data="data"
            :visible="true"
            ref="dialog"
          />
        </v-app>
      `,
    });

    wrapper = mount(App, {
      propsData: data,
      localVue,
      vuetify,
      attachTo: div,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  });

  describe('data: rules', () => {
    it('should highlight the input with an error message when the input field has the required rule set and it has not value', async () => {
      await wrapper.vm.$nextTick(); // we have to wait until vue
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.taskData.title = '';
      await wrapper.vm.$nextTick();
      await flushPromises();

      expect(wrapper.find('label[for="inputTitle"]').classes('error--text')).toBe(true);
    });

    it('should not highlight the input with an error message  when the input field has the required rule set and it has a value', async () => {
      await wrapper.vm.$nextTick();
      await flushPromises();

      expect(wrapper.find('label[for="inputTitle"]').classes('error--text')).toBe(false);
    });
  });


  describe('computed property: priorities', () => {
    it('should be assigned an array containing indexes from 1 to task priorityRange', async () => {
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;

      expect(wrapperDialog.priorities).toStrictEqual([ 1, 2, 3, 4, 5 ]);
    });
  });

  describe('watch: data', () => {
    it('watch: data y', async () => {
      await wrapper.vm.$nextTick(); // we have to wait until vue
      const componentDialog = wrapper.findComponent(TaskEditor);
      const wrapperDialog = wrapper.vm.$refs.dialog;
      await componentDialog.setProps({
        data: { title: 'new title' }
      });
      expect(wrapperDialog.taskData.title).toBe('new title');
    });
  });

  describe('method: onSubmit', () => {
    it('should return false when form data is not valid', async () => {
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.isValidForm = false;
      const isSubmitted = wrapperDialog.onSubmit();

      expect(isSubmitted).toBe(false);
    });

    it('should return false when form data is not valid', async () => {
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.isValidForm = false;
      const isSubmitted = wrapperDialog.onSubmit();

      expect(wrapperDialog.error).toBe('no_valid_data');
      expect(isSubmitted).toBe(false);
    });

    it('should submit task data successfully via POST method to creation API when task has no id', async () => {
      let response;
      localVue.prototype.$http = {
        post: jest.fn(() => {
          response = 'created'
          return Promise.resolve();
        }),
      };
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.isValidForm = true;  
      wrapperDialog.taskData.id = undefined;  
      wrapperDialog.onSubmit();
      await flushPromises();

      expect(response).toBe('created');
      localVue.prototype.$http = axiosInstance;
    });

    it('should submit task data successfully via PUT method to update API when task has id', async () => {
      let response;
      localVue.prototype.$http = {
        put: jest.fn(() => {
          response = 'updated'
          return Promise.resolve();
        }),
      };
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.isValidForm = true;  
      wrapperDialog.taskData.id = 1;  
      wrapperDialog.onSubmit();
      await flushPromises();
      
      expect(response).toBe('updated');
      localVue.prototype.$http = axiosInstance;
    });

    it('should empty error when task data is submitted successfully', async () => {
      localVue.prototype.$http = {
        post: jest.fn(() => Promise.resolve()),
        put: jest.fn(() => Promise.resolve()),
      };
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.isValidForm = true;
      wrapperDialog.taskData.id = undefined;
      wrapperDialog.onSubmit();
      await flushPromises();

      expect(wrapperDialog.error).toBe(null);
      localVue.prototype.$http = axiosInstance;
    });

    it('should set error to undefined when task data is submitted unsuccessfully to API and response is falsy', async () => {
      localVue.prototype.$http = {
        post: jest.fn(() => Promise.reject({ response: undefined })),
      };
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog;
      wrapperDialog.isValidForm = true;  
      wrapperDialog.taskData.id = undefined;
      wrapperDialog.onSubmit();
      await flushPromises();

      expect(wrapperDialog.error).toBe(undefined);
      localVue.prototype.$http = axiosInstance;
    });

    it('should set error to the value of response status when task data is submitted unsuccessfully to API and response is not falsy', async () => {
      localVue.prototype.$http = {
        post: jest.fn(() => Promise.reject({ response: { status: 403 } })),
      };
      await wrapper.vm.$nextTick();
      const wrapperDialog = wrapper.vm.$refs.dialog; 
      wrapperDialog.isValidForm = true;  
      wrapperDialog.taskData.id = undefined;
      wrapperDialog.onSubmit();
      await flushPromises();

      expect(wrapperDialog.error).toBe(403);
      localVue.prototype.$http = axiosInstance;
    });
  });
});
