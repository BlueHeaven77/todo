import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import App from '@/App.vue';

describe('App.vue', () => {
  let localVue;
  let vuetify;
  let wrapper;
  const div = document.createElement('div');

  beforeAll(() => {
    localVue = createLocalVue(); // because of vuetify, we should use a localVue instance
    vuetify = new Vuetify();
    document.body.appendChild(div);

    wrapper = mount(App, {
      localVue,
      vuetify,
      stubs: ['RouterView', 'router-view'],
      attachTo: div,
    });
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  })
});
