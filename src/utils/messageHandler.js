function mapHttpError(error) {
  const errCode = error === undefined ? 'no_conn' : error;
  return {
    no_conn: 'Connection Refused',
    400: 'Verify your data',
    401: 'Credentials are not correct',
    403: 'Not authorized',
    422: 'Account already exists',
  }[errCode];
}

function formValidationError(error) {
  const errCode = error;
  return {
    no_valid_data: 'Please fill correctly the data',
  }[errCode];
}

export default {
  mapHttpError,
  formValidationError,
};
