import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '@/views/Login.vue';
import Signup from '@/views/Signup.vue';
import Tasks from '@/views/Tasks.vue';

Vue.use(VueRouter);

const checkAuthentication = (to, from, next) => {
  if (localStorage.getItem('jwt')) {
    next();
    return;
  }

  next('/login');
};

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      title: 'ToDo App | Login',
    },
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup,
    meta: {
      title: 'ToDo App | Signup',
    },
  },
  {
    path: '/',
    name: 'Tasks',
    component: Tasks,
    meta: {
      title: 'ToDo App | Task List',
    },
    beforeEnter: checkAuthentication,
  },
  {
    path: '*',
    name: 'HttpError404',
    component: () => import(/* webpackChunkName: "HttpError404" */ '@/views/HttpErrors/404.vue'),
    meta: {
      title: 'ToDo App - Page not found (404)',
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || 'ToDo App';
  next();
});

export default router;
