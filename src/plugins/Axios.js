import axios from 'axios';

// Hardcoded values are okay for a sample app
const axiosInstance = axios.create({ baseURL: 'http://localhost:3000/' });

function requestHandler(request) {
  const jwt = localStorage.getItem('jwt');

  if (jwt) {
    request.headers.Authorization = `Bearer ${jwt}`;
  }

  return request;
}

function errorHandler(error) {
  if (+error.response.status === 401) {
    localStorage.removeItem('jwt');
  }

  return Promise.reject(error);
}

function successHandler(response) {
  if (response.data.token) {
    localStorage.setItem('jwt', response.data.token);
  }

  return response;
}

axiosInstance.interceptors.request.use(requestHandler);
axiosInstance.interceptors.response.use(successHandler, errorHandler);

export default {
  install(Vue) {
    Vue.prototype.$http = axiosInstance;
  },
};
