import Vue from 'vue';

import AxiosPlugin from '@/plugins/Axios';
import Vuetify from '@/plugins/Vuetify';

import App from '@/App.vue';
import router from '@/router';
import store from '@/store';

Vue.config.productionTip = false;

Vue.use(AxiosPlugin);

new Vue({
  router,
  store,
  vuetify: Vuetify,
  render: (h) => h(App),
}).$mount('#app');
