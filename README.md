# To-do List Web App
A Vue application for simple task management.

## Project setup
This is the front-end setup, but the application requires a back-end setup in order to work (see server repo for further details). 
Once the server is up and running, it will listen on http://localhost:3000.

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```
The coverage report is generated under the folder coverage (open on .\coverage\lcov-report\index.html)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Notes
As expected the application is not 100% complete, working and polished.
In particular:

- Organization of views and components can be improved.
- Logout function is missing. When the login fails the authentication is token is cleared out.
- When login session expires there is no autoredirect to login. Go manually to http://localhost:8080/login.
- Many variable checks, edge cases, exceptions and errors are not handled.
- Message handling and display of responses and errors returned by the server is partial, please refer to the console.
- A dictionary system for the text or a text internationalization functionality are missing (text is hard-coded).
- The task priority is handle as part of the task data, but there was no time to build a functionality around it. As the name suggests, it was meant to provide a custom sorting criteria to the user.
- Test coverage is demonstrative and covers fully only the most meaningful components and views.

## Techology stack

### Core
- Vue
- Vue Router
- Vue Axios

### Cosmetics and UI
- Vuetify

### Unit testing
- Jest
